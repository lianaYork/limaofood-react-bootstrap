const APIV1 = '/api/v1'

module.exports = {
  name: 'Limaofood',
  prefix: 'limaoFood',
  footerText: 'Copyright © 2018 Limaofood',
  host: 'www.limaofood.com',
  logo: '/logo.svg',
  iconFontCSS: '/iconfont.css',
  iconFontJS: '/iconfont.js',
  CORS: [],
  openPages: ['/auth'],
  apiPrefix: '/api/v1',
  APIV1,
  api: {
    userLogin: `${APIV1}/auth/login`,
    userLogout: `${APIV1}/auth/logout`,
    userInfo: `${APIV1}/userInfo`,
    users: `${APIV1}/auth`,
    posts: `${APIV1}/posts`,
    user: `${APIV1}/user/:id`,
    dashboard: `${APIV1}/dashboard`,
    menus: `${APIV1}/menus`,
    weather: `${APIV1}/weather`,
    v1test: `${APIV1}/test`
  },
}
