import { userConstants } from '../constants'

let user = localStorage.getItem('user')
const initialState = user ? { loggedIn: false, user } : {}

export function authentication (state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: false,
        loggedIn: false,
        user: action.user
      }
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      }
    case userConstants.LOGIN_FAILURE:
      return {
        loggedIn: false
      }
    case userConstants.LOGOUT:
      return {}
    default:
      return state
  }
}