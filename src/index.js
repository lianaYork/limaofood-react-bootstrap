import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store } from 'helpers';

import registerServiceWorker from './registerServiceWorker'
import { unregister } from './registerServiceWorker'
import Router from './router'

import './index.css'

unregister()
ReactDOM.render(
    <Provider store={store}>
        <Router />
    </Provider>, document.getElementById('root'))
registerServiceWorker()
