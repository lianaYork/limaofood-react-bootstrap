import NOTFOUND from './NOTFOUND'
import Auth from './Auth'
import Logout from './Logout'
import Register from './Register'
import Home from './Home'
import Partner from './Partner'
import Explore from './Explore'
import Events from './Events'

export {
    NOTFOUND,
    Auth,
    Logout,
    Register,
    Home,
    Partner,
    Explore,
    Events
}
