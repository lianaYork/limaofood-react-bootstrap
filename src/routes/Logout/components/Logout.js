import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { userActions } from '../../../actions'

class Login extends Component {
    constructor(props) {
        super(props)
        this.props.dispatch(userActions.logout())
    }

    render () {
        return (
            <div>
                <div className="col-xs-12 logout-text-center">
                    <h2>Thank you for your visit</h2>
                    <NavLink to="/home">Back to home</NavLink>
                </div>
            </div>
        )
    }
}

Login.propTypes = {
    credential: PropTypes.object,
    actions: PropTypes.object
}
function mapStateToProps (state) {
    const { loggingIn } = state.authentication
    return {
        loggingIn
    }
}

export default connect(mapStateToProps)(Login)