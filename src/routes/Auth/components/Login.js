import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, FormControl, FormGroup, Col, ControlLabel, Button } from 'react-bootstrap'
import validator from 'validator'

import { userActions } from '../../../actions'

class Login extends Component {
    constructor(props) {
        super(props)
        // this.props.dispatch(userActions.logout())
        this.state = {
            email: { value: '', isValid: true, message: '' },
            password: { value: '', isValid: true, message: '' }
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange (e) {
        const { id, value } = e.target
        let state = this.state
        state[id].value = value

        this.setState(state)
    }

    handleSubmit (e) {
        e.preventDefault()

        if (this.formIsValid()) {
            this.setState({ submitted: true })
            const { email, password } = this.state
            const { dispatch } = this.props
            if (email.value && password.value) {
                dispatch(userActions.login(email.value, password.value))
            }
        }
    }

    formIsValid = () => {
        let state = this.state
        if (!validator.isEmail(state.email.value)) {
            state.email.isValid = false
            state.email.message = 'Not a valid email address'

            this.setState(state)
            return false
        } else {
            state.email.isValid = true
            state.email.message = ''

            this.setState(state)
        }
        if (state.password.value === '' || state.password.value === null) {
            state.password.isValid = false
            state.password.message = 'Password is require'

            this.setState(state)
            return false
        } else {
            state.password.isValid = true
            state.password.message = ''

            this.setState(state)
        }

        //additional validation checks here...

        return true
    }

    resetValidationStates = () => {
        let state = this.state

        Object.keys(state).map(key => {
            if (state[key].hasOwnProperty('isValid')) {
                state[key].isValid = true
                state[key].message = ''
            }
            return state[key]
        })
        this.setState(state)
    }

    render () {
        const { email, password } = this.state
        let emailGroupClass = classNames('form-group', { 'has-error': !email.isValid })
        let passwordGroupClass = classNames('form-group', { 'has-error': !password.isValid })
        return (
            <div className="bg-green">
                <div className="containers card">
                    <div className="card-header">
                        <h3>Login</h3>
                    </div>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup controlId="email" className={emailGroupClass}>
                            <Col componentClass={ControlLabel} sm={4}>
                                Email
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="email"
                                    placeholder="Email"
                                    value={(email || '').value}
                                    onChange={this.handleChange}
                                    autoFocus
                                />
                                <span className="help-block">{(email || '').message}</span>
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="password" className={passwordGroupClass}>
                            <Col componentClass={ControlLabel} sm={4}>
                                Password
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="password"
                                    placeholder="Password"
                                    value={(password || '').value}
                                    onChange={this.handleChange}
                                />
                                <span className="help-block">{(password || '').message}</span>
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={2} sm={8}>
                                <Button type="submit" bsStyle="primary" onClick={this.handleSubmit}>Sign in</Button>
                            </Col>
                            <Col smOffset={2} sm={8}>
                                Need an account? <Link to="/signup">Sign up</Link>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        )
    }
}

Login.propTypes = {
    credential: PropTypes.object,
    actions: PropTypes.object
}
function mapStateToProps (state) {
    const { loggingIn } = state.authentication
    return {
        loggingIn
    }
}

export default connect(mapStateToProps)(Login)