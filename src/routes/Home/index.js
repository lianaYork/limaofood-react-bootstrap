import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Jumbotron } from 'react-bootstrap'
import './style.css'
class Home extends Component {
    render () {
        return (
            <div className="jumbotron">
                <Jumbotron>
                    <h1>Limaofood</h1>
                    <h2>All you can eat</h2>
                </Jumbotron>
            </div>
        )
    }
}

export default connect()(Home)