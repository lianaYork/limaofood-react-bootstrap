import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Jumbotron } from 'react-bootstrap'
class Partner extends Component {
    render () {
        return (
            <div className="jumbotron">
                <Jumbotron>
                    <h1>Partner</h1>
                    <h2>Enjoy limaofood partnership</h2>
                </Jumbotron>
            </div>
        )
    }
}

export default connect()(Partner)