import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form, FormControl, FormGroup, Col, ControlLabel, Button } from 'react-bootstrap'
import validator from 'validator'

import { userActions } from '../../../actions'

class Register extends Component {
    constructor(props) {
        super(props)

        this.state = {
            credential: {
                name: { value: '', isValid: true, message: '' },
                email: { value: '', isValid: true, message: '' },
                password: { value: '', isValid: true, message: '' }
            }
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit (e) {
        e.preventDefault()
        console.log('e', e)
        if (this.formIsValid()) {
            this.setState({ submitted: true })
            const { credential } = this.state
            const { dispatch } = this.props
            if (credential.name.value && credential.email.value && credential.password.value) {
                const data = {
                    name: credential.name.value,
                    email: credential.email.value,
                    password: credential.password.value
                }
                dispatch(userActions.register(data))
            }
        }
    }

    handleChange (e) {
        const { id, value } = e.target
        let state = this.state
        state.credential[id].value = value

        this.setState(state)
    }

    formIsValid = () => {
        let state = this.state
        if (!validator.isEmail(state.credential.email.value)) {
            state.credential.email.isValid = false
            state.credential.email.message = 'Not a valid email address'

            this.setState(state)
            return false
        } else {
            state.credential.email.isValid = true
            state.credential.email.message = ''

            this.setState(state)
        }
        if (state.credential.password.value === '' || state.credential.password.value === null) {
            state.credential.password.isValid = false
            state.credential.password.message = 'Password is require'

            this.setState(state)
            return false
        } else {
            state.credential.password.isValid = true
            state.credential.password.message = ''

            this.setState(state)
        }

        //additional validation checks here...

        return true
    }

    resetValidationStates = () => {
        let state = this.state

        Object.keys(state).map(key => {
            if (state.credential[key].hasOwnProperty('isValid')) {
                state.credential[key].isValid = true
                state.credential[key].message = ''
            }
            return state.credential[key]
        })
        this.setState(state)
    }

    render () {
        const { name, email, password } = this.state.credential
        let nameGroupClass = classNames('form-group', { 'has-error': !password.isValid })
        let emailGroupClass = classNames('form-group', { 'has-error': !email.isValid })
        let passwordGroupClass = classNames('form-group', { 'has-error': !name.isValid })
        return (
            <div className="bg-green">
                <div className="containers card">
                    <div className="card-header">
                        <h3>Create your account</h3>
                    </div>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup controlId="name" className={nameGroupClass}>
                            <Col componentClass={ControlLabel} sm={4}>
                                Fullname
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="text"
                                    placeholder="Fullname"
                                    value={(name || '').value}
                                    onChange={this.handleChange}
                                    autoFocus
                                />
                            </Col>
                            <span className="help-block">{(name || '').message}</span>
                        </FormGroup>

                        <FormGroup controlId="email" className={emailGroupClass}>
                            <Col componentClass={ControlLabel} sm={4}>
                                Email
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="email"
                                    placeholder="Email"
                                    value={(email || '').value}
                                    onChange={this.handleChange}
                                />
                                <span className="help-block">{(email || '').message}</span>
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="password" className={passwordGroupClass}>
                            <Col componentClass={ControlLabel} sm={4}>
                                Password
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="password"
                                    placeholder="Password"
                                    value={(password || '').value}
                                    onChange={this.handleChange}
                                />
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={2} sm={8}>
                                <Button type="submit" bsStyle="primary">Sign up</Button>
                            </Col>
                            <Col smOffset={2} sm={8}>
                                Already have account? <Link to="/auth">Login</Link>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        )
    }
}

Register.propTypes = {
    credential: PropTypes.object,
    actions: PropTypes.object
}

function mapStateToProps (state) {
    const { registering } = state.authentication
    return {
        registering
    }
}

export default connect(mapStateToProps)(Register)