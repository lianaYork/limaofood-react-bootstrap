import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Jumbotron } from 'react-bootstrap'
class Explore extends Component {
    render () {
        return (
            <div className="jumbotron">
                <Jumbotron>
                    <h1>Explore</h1>
                    <h2>Explore new taste of culinary</h2>
                </Jumbotron>
            </div>
        )
    }
}

export default connect()(Explore)
