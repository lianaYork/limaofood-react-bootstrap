import { userConstants } from '../constants'
import { login as loginService, logout as logoutService, me as userInfo, register as registerService } from 'services'
import { alertActions } from './'
import { history } from '../helpers'

export const userActions = {
    me,
    login,
    logout,
    register,
    // getAll,
    // delete: _delete
}

// async function me () {
//     console.log('user1')
//     return dispatch => {

//         userInfo()
//             .then(
//                 user => {
//                     console.log('user', user)
//                     dispatch(success(user))
//                 },
//                 error => {
//                     dispatch(failure(error))
//                 }
//             )
//     }
//     function success (user) { return { type: userConstants.LOGIN_SUCCESS, user } }
//     function failure (error) { return { type: userConstants.LOGIN_FAILURE, error } }
// }

function me () {
    return dispatch => {
        dispatch(request({}))

        userInfo()
            .then(
                user => {
                    if (user) {
                        dispatch(success(localStorage.getItem('user')))
                    }
                },
                error => {
                    dispatch(failure(error))
                }
            )
    }

    function request (user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success (user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure (error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function login (email, password) {
    return dispatch => {
        dispatch(request({ email }))

        loginService(email, password)
            .then(
                user => {
                    if (user) {
                        dispatch(success(user))
                        localStorage.setItem('user', user.token)
                        history.push('/')
                    }
                },
                error => {
                    console.log('error', error)
                    dispatch(failure(error))
                    dispatch(alertActions.error(error.message))
                }
            )
    }

    function request (user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success (user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure (error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout () {
    logoutService()
    return { type: userConstants.LOGOUT }
}

function register (user) {
    return dispatch => {
        dispatch(request(user))

        registerService(user)
            .then(
                user => {
                    dispatch(success())
                    history.push('/auth')
                    dispatch(alertActions.success('Registration successful'))
                },
                error => {
                    console.log('error', error)
                    dispatch(failure(error.message))
                    dispatch(alertActions.error(error.message))
                }
            )
    }

    function request (user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success (user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure (error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

// function getAll () {
//     return dispatch => {
//         dispatch(request())

//         userService.getAll()
//             .then(
//                 users => dispatch(success(users)),
//                 error => dispatch(failure(error))
//             )
//     }

//     function request () { return { type: userConstants.GETALL_REQUEST } }
//     function success (users) { return { type: userConstants.GETALL_SUCCESS, users } }
//     function failure (error) { return { type: userConstants.GETALL_FAILURE, error } }
// }

// prefixed function name with underscore because delete is a reserved word in javascript
// function _delete (id) {
//     return dispatch => {
//         dispatch(request(id))

//         userService.delete(id)
//             .then(
//                 user => {
//                     dispatch(success(id))
//                 },
//                 error => {
//                     dispatch(failure(id, error))
//                 }
//             )
//     }

//     function request (id) { return { type: userConstants.DELETE_REQUEST, id } }
//     function success (id) { return { type: userConstants.DELETE_SUCCESS, id } }
//     function failure (id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
// }