import React, { Component } from 'react'
// import { BrowserRouter } from 'react-router-dom'
import { Router } from 'react-router'
import { history } from '../../helpers'
import Header from './Header'

class Routes extends Component {
    render () {
        return (
            <Router history={history} routes={this.props.children}>
                <div>
                    <Header />
                    {this.props.children}
                </div>
            </Router>
        )
    }
}

export default Routes