import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'

import { history } from 'helpers'
import { alertActions } from 'actions'
import { NOTFOUND, Auth, Logout, Register, Home, Partner, Explore, Events } from '../../routes'
import Routes from './Routes'
import withTracker from './withTracker'

const routes = (props) => {
    return (
        <Switch history={history}>
            <Route exact path="/" component={withTracker(Home, 'Home')} />

            <Route exact path="/auth" component={withTracker(Auth, 'Login')} />
            {(props.loggedIn || localStorage.getItem('user') === null) && <Route exact path="/logout" component={withTracker(Logout, 'Logout')} />}
            <Route exact path="/me" component={withTracker(Home, 'Home')} />
            <Route exact path="/signup" component={withTracker(Register, 'Home')} />
            <Route exact path="/recovery" component={withTracker(Home, 'Home')} />
            <Route exact path="/reset_password" component={withTracker(Home, 'Home')} />
            <Route exact path="/reset" component={withTracker(Home, 'Home')} />

            <Route exact path="/home" component={withTracker(Home, 'Home')} />
            <Route exact path="/partner" component={withTracker(Partner, 'Partner')} />
            <Route exact path="/explore" component={withTracker(Explore, 'Explore')} />
            <Route exact path="/events" component={withTracker(Events, 'Events')} />
            <Route exact path="*" component={NOTFOUND} />
        </Switch>)
}

class GlobalNav extends Component {
    constructor(props) {
        super(props)

        const { dispatch } = this.props
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear())
        })
    }
    render () {
        return (
            <div>
                <Routes>
                    {routes(this.props)}
                </Routes>
            </div>
        )
    }
}
function mapStateToProps (state) {
    const { authentication } = state
    const { user, loggedIn } = authentication
    return {
        user,
        loggedIn
    }
}
export default connect(mapStateToProps)(GlobalNav)