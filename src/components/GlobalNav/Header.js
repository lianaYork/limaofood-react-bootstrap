import React, { Component } from 'react'
import Headroom from 'react-headroom'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { Navbar } from 'react-bootstrap'
import { userActions } from '../../actions'

import { alertActions } from '../../actions'

class Header extends Component {
    constructor(props) {
        super(props)

        this.closeAlerts = this.closeAlerts.bind(this)
    }
    closeAlerts () {
        const { dispatch } = this.props
        dispatch(alertActions.clear())
    }
    componentDidMount () {
        localStorage.getItem('user') && this.props.dispatch(userActions.me())
    }
    render () {
        const { alert } = this.props
        return (
            <div>
                <Headroom>
                    <Navbar fixedTop className="xnavbar">
                        <Navbar.Header className="header">
                            <Navbar.Brand>
                                <NavLink to="/">
                                    Limaofood
                            </NavLink>
                            </Navbar.Brand>
                        </Navbar.Header>
                        <div className="menu">
                            <NavLink to="/home">Home</NavLink>
                            <NavLink to="/partner">Partner</NavLink>
                            <NavLink to="/explore">Explore</NavLink>
                            <NavLink to="/events">Event</NavLink>
                            {!this.props.loggedIn ? <NavLink to="/auth">Login</NavLink> : <NavLink to="/logout">Logout</NavLink>}
                        </div>
                    </Navbar>
                </Headroom>
                {alert.message &&
                    <div style={{ margin: '0px' }} className={`alert ${alert.type}`}>
                        <div className="close" onClick={this.closeAlerts} data-dismiss="alert" aria-label="close">&times;</div>
                        {alert.message}
                    </div>
                }
            </div>
        )
    }
}

function mapStateToProps (state) {
    const { users, authentication, alert } = state
    const { user, loggedIn } = authentication
    return {
        user,
        loggedIn,
        users,
        alert
    }
}
export default connect(mapStateToProps)(Header)