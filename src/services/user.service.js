import { authHeader } from '../helpers'
import utils from 'utils'
const { request } = utils

export function me () {
    return request({
        url: 'http://localhost:8000/api/auth/me',
        method: 'get',
        headers: authHeader()
    })
}

export function login (email, password) {
    return request({
        url: 'http://localhost:8000/api/auth/login',
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        data: {
            email: email,
            password: password
        },
    })
}

export function register (data) {
    return request({
        url: 'http://localhost:8000/api/auth/signup',
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        data: data,
    })
}

// function login (email, password) {
//     const requestOptions = {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json' },
//         body: JSON.stringify({ email, password })
//     }

//     return fetch('http://localhost:8000/api/auth/login', requestOptions)
//         .then(response => {
//             if (!response.ok) {
//                 return Promise.reject(response.statusText)
//             }
//             return response.json()
//         })
//         .then(user => {
//             // login successful if there's a jwt token in the response
//             if (user && user.token) {
//                 // store user details and jwt token in local storage to keep user logged in between page refreshes
//                 localStorage.setItem('user', user.token)
//             }

//             return user
//         })
// }

export function logout () {
    // remove user from local storage to log user out
    if (Object.assign(authHeader()).length > 0) {
        const requestOptions = {
            method: 'POST',
            headers: { ...authHeader() },
        }

        return fetch('http://localhost:8000/api/auth/logout', requestOptions)
            .then(response => {
                if (!response.ok) {
                    return Promise.reject(response.statusText)
                }

                return response.json()
            })
            .then(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', user.token)
                }

                return user
            })
    }
    localStorage.removeItem('user')
}

// function getAll () {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     }

//     return fetch('/users', requestOptions).then(handleResponse)
// }

// function getById (id) {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     }

//     return fetch('/users/' + id, requestOptions).then(handleResponse)
// }

// export function register (user) {
//     const requestOptions = {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json' },
//         body: JSON.stringify(user)
//     }

//     return fetch('/users/register', requestOptions).then(handleResponse)
// }

// function update (user) {
//     const requestOptions = {
//         method: 'PUT',
//         headers: { ...authHeader(), 'Content-Type': 'application/json' },
//         body: JSON.stringify(user)
//     }

//     return fetch('/users/' + user.id, requestOptions).then(handleResponse)
// }

// // prefixed function name with underscore because delete is a reserved word in javascript
// function _delete (id) {
//     const requestOptions = {
//         method: 'DELETE',
//         headers: authHeader()
//     }

//     return fetch('/users/' + id, requestOptions).then(handleResponse)
// }

// function handleResponse (response) {
//     if (!response.ok) {
//         return Promise.reject(response.statusText)
//     }

//     return response.json()
// }