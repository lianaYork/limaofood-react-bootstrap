import React, { Component } from 'react'
import { GlobalNav } from 'components'
import './router.css'

class App extends Component {
    render () {
        return (
            <div>
                <GlobalNav />
            </div>
        )
    }
}
export default App