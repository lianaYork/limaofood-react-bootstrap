import jwtDecode from 'jwt-decode'

let user = localStorage.getItem('user')

export function isLoggedIn () {
    // Jika tidak ada local dan expired
    if (user) {
        let isExpired = false
        let decodedToken = jwtDecode(user)
        var dateNow = new Date()
        if (decodedToken.exp < dateNow.getTime() / 1000) {
            isExpired = false
        } else {
            isExpired = true
        }
        return isExpired
    } else {
        return false
    }
}